hostname TermServ
!
enable password cisco
!
no ip domain lookup
!
! 169.254.0.1 should be the IP address of your PC's Loopback
!
 ip host R1 2001 169.254.0.1
 ip host R2 2002 169.254.0.1
 ip host R3 2003 169.254.0.1
 ip host R4 2004 169.254.0.1
 ip host R5 2005 169.254.0.1
 ip host R6 2006 169.254.0.1
ip host SW1 2007 169.254.0.1
ip host SW2 2008 169.254.0.1
ip host SW3 2009 169.254.0.1
ip host SW4 2010 169.254.0.1
ip host BB1 2011 169.254.0.1
ip host BB2 2012 169.254.0.1
ip host BB3 2013 169.254.0.1
!
interface Ethernet0/0
 ip address 169.254.0.2 255.255.0.0
!
ip classless
!
ip route 0.0.0.0 0.0.0.0 169.254.0.1
!
line vty 0 4
 no login
!
end