OSPF NSSA LSA TYPE 7 TO 5 TRANSLATOR ELECTION
WRITTEN BY RENE MOLENAAR ON 20 SEPTEMBER 2011. POSTED IN OSPF
SCENARIO:
You run a company that sells Virtual Worlds for gamers. The network you are using is running on OSPF and one of the areas is a NSSA. At the moment you are not sure which router is performing the translation from LSA Type 7 to Type 5 into your backbone router. Up to you to check it out!
GOAL:
* All IP addresses have been preconfigured for you.
* Configure OSPF Area 0.
* Configure OSPF Area 1 as NSSA.
* Redistribute the loopback0 interface on router Sam into OSPF Area 1.
* Ensure router Tron is the router performing the translation from LSA Type 7 to Type 5 into area 0.
IOS:
c3640-jk9s-mz.124-16.bin

