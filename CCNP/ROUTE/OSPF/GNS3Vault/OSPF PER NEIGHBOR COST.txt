OSPF PER NEIGHBOR COST
WRITTEN BY RENE MOLENAAR ON 20 SEPTEMBER 2011. POSTED IN OSPF
SCENARIO:
The local fish trading market needs your help with their OSPF network. It seems their routers are unable to change the bandwidth or cost on the interfaces so you need to use another solution to influence OSPF routing...sounds fishy!
GOAL:
* All IP addresses have been preconfigured for you.
* Configure OSPF on all routers. Achieve full connectivity.
* Configure a loopback0 interface with IP address 1.1.1.1 /32 on router Salmon and Herring.
* Advertise the loopback0 interface on router Salmon and Herring.
* Configure your network so router Barracuda sends all traffic for 1.1.1.1 /32 to router Herring. You are not allowed to change the bandwidth or cost on the interface.
IOS:
c3640-jk9s-mz.124-16.bin

